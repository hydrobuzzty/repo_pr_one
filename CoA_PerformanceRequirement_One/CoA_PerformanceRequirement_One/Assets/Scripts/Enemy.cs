﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using PROne;
using UnityEditor.Experimental.UIElements;
using UnityEditor.VersionControl;
using UnityEngine;

public class Enemy : Entity, IDamagable
{
    [SerializeField] private int currentHealth;
    [SerializeField] private int attackAmount;
    [SerializeField] private bool isAlive;
    
    public int CurrentHealth
    {
        get { return currentHealth; }
        set { currentHealth = value; }
    }
    public int AttackAmount
    {
        get { return attackAmount; }
        set { attackAmount = value; }
    }
    public bool IsAlive
    {
        get { return isAlive; }
        set { isAlive = value; }
    }

    public void Damage(int amount)
    {
        if (currentHealth > 0)
        {
            currentHealth -= amount;
            
            if (currentHealth <= 0)
            {
                currentHealth = 0;
                isAlive = false;
            
                Log(EntityName + currentHealth);

                return;
            }
        }

        if (currentHealth <= 0)
        {
            currentHealth = 0;
            isAlive = false;

            Log(EntityName + currentHealth);
            return;
        }
        Log(EntityName + "has been damaged. His current health is:" +  currentHealth);
    }
    
    
    
    
}

