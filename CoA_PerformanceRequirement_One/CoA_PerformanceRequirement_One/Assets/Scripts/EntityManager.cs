﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class EntityManager : MonoBehaviour, ILog
    {
        public void Log(object message)
        {
            Debug.Log(message);
        }

        public Entity[] entities;

        public void Start()
        {
            Log("Names are being registere...please wait!");

            for (int i = 0; i < entities.Length; i++)
            {
                Log(entities[i]);
            }
        }
    }

    
}


